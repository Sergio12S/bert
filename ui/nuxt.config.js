import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - Bert',
    title: 'Similar text',

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],

  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins:
    [
    ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  // axios: {
  //   baseURL: 'localhost',
  //   proxy: false
  // },
  axios: {
    baseURL: 'http://localhost/api',
    proxy: false
  },
  // Develelopng axios
  // axios: {
  //   baseURL: 'http://localhost',
  //   proxy: true
  // },
  // proxy: {
  //   '/api/': {
  //     target: 'http://localhost:5000',
  //     pathRewrite: {
  //       'http://localhost:3000/api/': 'http://localhost:5000/api/'
  //     }
  //   }
  // },
  vuetify: {
    optionsPath: './vuetify.options.js',
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    parallel: true,
    cache: true,
    hardSource: true,
    extend(config, ctx) {
    }
  }
}
