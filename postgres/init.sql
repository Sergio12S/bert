CREATE TABLE public.text (
  id serial PRIMARY KEY,
  title TEXT,
  text TEXT,
  url VARCHAR(50)
);