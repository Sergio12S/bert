from .extensions import cors, ma, db
from .commands import create_tables
from flask import Flask
from flask_patterns.routes.text import text
from flask_patterns.routes.ber import bert


def create_app(config_file="settings.py"):
    app = Flask(__name__)
    app.config.from_pyfile(config_file)
    cors.init_app(app, resources={r"/*": {"origins": "http://127.0.0.1:3000"}})
    ma.init_app(app)
    app.register_blueprint(text)
    app.register_blueprint(bert)
    app.cli.add_command(create_tables)
    db.init_app(app)
    return app
