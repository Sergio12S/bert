
from flask import Blueprint, request, jsonify
from bert_serving.client import BertClient
from flask_patterns.models import Text
import pandas as pd
import numpy as np
from nltk import tokenize

bc = BertClient(ip="95.217.158.183")
bert = Blueprint("bert", __name__)


@bert.route("/api/bert", methods=["GET"])
def similar():
    '''
    Search similar text using BERT
    '''
    # Part 1. Data preprocessing for analyse text
    df = Text.query.with_entities(
        Text.title,
        Text.text,
        Text.url
    ).all()
    if not df:
        return jsonify({"text": 'no data'})

    df = pd.DataFrame(df)
    df_pre = pd.DataFrame()
    # Tokenize all text
    for i, k in df.iterrows():
        token = tokenize.sent_tokenize(k['text'])
        for i in token:
            temp = pd.DataFrame({
                'sentences': i,
                'title': k['title'],
                'url': k["url"]
            }, index=[0])
            df_pre = pd.concat(
                [df_pre, temp],
                sort=True
            )
    # Tokenize current text from UI
    query = tokenize.sent_tokenize(request.args.get("sentence"))
    topk = int(request.args.get('top'))

    # Create texts vecrots
    doc_vecs = bc.encode(df_pre['sentences'].tolist())
    query_vec = bc.encode(query)[0]

    # compute normalized dot product as score
    score = np.sum(query_vec * doc_vecs, axis=1) / \
        np.linalg.norm(doc_vecs, axis=1)

    # N top the most similar text
    if topk > len(score)-1:
        topk = len(score)-1
    # Sort top most similar text
    topk_idx = np.argsort(score)[::-1][:topk]
    similar_sentences = pd.DataFrame({
        'sentences': np.array(df_pre['sentences'].tolist())[topk_idx],
        'title': np.array(df_pre['title'].tolist())[topk_idx],
        # returl url for text
        'url': np.array(df_pre['url'].tolist())[topk_idx],
        # return similar scoreu
        'score': np.sort(score)[::-1][:topk]
    })
    return jsonify({"similar":  similar_sentences.to_dict(orient="records")})
