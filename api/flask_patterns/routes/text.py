from flask import Blueprint, request, jsonify
from flask_patterns.models import Text, db
from nltk import tokenize
import pandas as pd
import random
import string
import sys

text = Blueprint("text", __name__)


def generatorSymb(stringLength=10):
    '''
    generate random label fore create url
    '''
    genSymb = ''.join([
        random.choice(string.ascii_letters +
                      string.digits) for n in range(int(stringLength))])

    return genSymb


@text.route("/api/text", methods=["POST"])
def write_text():
    '''
    ad new text to data base
    '''
    data = request.get_json()
    # Chec file. Must be less than 1 Mb.
    if sys.getsizeof(data) > 1000000:
        return jsonify({"message": "Text must be less than 1Mb"})
    url = generatorSymb()
    newText = Text(
        text=data.get('text'),
        title=data.get('title'),
        url=url
    )
    db.session.add(newText)
    db.session.commit()

    return jsonify({"message": "Text has been added",
                    'url': url})


@text.route("/api/text", methods=["GET"])
def read_text():
    '''
    Get text from data base for frontend
    '''
    if request.args.get("type") == 'analyse':
        # Get tokenize text for analyse sentences
        data = Text.query.filter_by(
            url=request.args.get("url")
        ).with_entities(
            Text.text,
        ).all()
        if not data:
            return jsonify({"text": 'no data'})

        data = pd.DataFrame(data)
        data = tokenize.sent_tokenize(data.values[0][0])
        return jsonify({"text": data})

    if request.args.get("type") == "list":
        # Get list of texts
        data = Text.query.with_entities(
            Text.title,
            Text.url
        ).all()
        if not data:
            return jsonify({"text": 'no data'})
        data = pd.DataFrame(data)
        return jsonify({"texts": data.to_dict(orient="records")})
