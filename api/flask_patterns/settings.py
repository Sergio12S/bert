import os

SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
SECRET_KEY = os.environ.get("SECRET_KEY")
PUBLIC_KEY = os.environ.get("PUBLIC_KEY")
SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
    "SQLALCHEMY_TRACK_MODIFICATIONS")
DEBUG = os.environ.get("DEBUG")
TESTING = os.environ.get("TESTING")
