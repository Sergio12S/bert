from .extensions import db


class Text(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    text = db.Column(db.Text)
    url = db.Column(db.String(50))
