import click
from flask.cli import with_appcontext
from .extensions import db
from .models import Text


@click.command(name="create_tables")
@with_appcontext
def create_tables():
    '''
    create comand for buildint table for data base
    flask create_tables
    '''
    db.create_all()
