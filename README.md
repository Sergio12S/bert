# README #
Run aplication at 'http://localhost/'


### What is this repository for? ###

* Quick summary
The BERT algorithm was developed by the Google team and is one of the most effective algorithms for searching for text similarity.
This is an example application of how the BERT algorithm searches for concise sentences in a text and displays a sorted list of similar sentences from other texts.
* Version
Version 1.0

### How do I get set up? ###

* Summary of set up
* Deploy application
```
docker-compose build
docker-compose up -d
```

* Run server BERT
At the moment, the BERT algorithm is uploaded to our remote server `95.217.158.183`, which you can use for your own purposes.
If you want to run the BERT algorithm on your remote server, use these commands:
```
docker-compose -f docker-compose.bert.yml build
docker-compose -f docker-compose.bert.yml up -d
```

* Dependencies
`pip install -r requirements.txt`

### FAQ ###

* Close ports
To start docker containers, you need available ports for the database and nginx:

```
sudo lsof -i :5432
sudo service postgresql stop
sudo service nginx stop
```
* Writing tests
* Code review
* Data base
`http://localhost:8080/`
